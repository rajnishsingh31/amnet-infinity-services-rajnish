package org.miq.infinity.spc.util;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwnerDo;
import org.miq.infinity.spc.domain.MediaOwners;
import org.miq.infinity.spc.domain.MediaOwnersDo;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.exception.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;

@Component
@Configuration
@PropertySource("classpath:application.properties")
public class SpcUtil {

    private static final Logger LOG = LoggerFactory.getLogger(SpcUtil.class);
    public static final char FILE_SEPERATOR = ',';
    public static final String FILE_EMPTY_MESSAGE = "File is Empty, Please upload correct file";
    public static final String FILE_TYPE_MESSAGE = "Please upload .csv or .xlsx file";
    public static final String FILE_SUCCESS_MESSAGE = "File is uploaded and saved successfully in database";

    private MediaOwners parseUploadedExcelFile(MultipartFile uploadFile) throws IOException {
    	MediaOwnersDo mediaOwnersAndPriorities = new MediaOwnersDo();
    	MediaOwnerDo mediaOwner = new MediaOwnerDo();
    	List<MediaOwner> mediaOwnersList = new ArrayList<>();
        try (InputStream inputStream = new BufferedInputStream(uploadFile.getInputStream());
                XSSFWorkbook workbook = new XSSFWorkbook(inputStream);) {
            Sheet firstSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = firstSheet.iterator();

            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                if (nextRow.getRowNum() > 0) {
                    Iterator<Cell> cellIterator = nextRow.cellIterator();
                    mediaOwner = new MediaOwnerDo();

                    while (cellIterator.hasNext()) {
                        Cell nextCell = cellIterator.next();
                        int columnIndex = nextCell.getColumnIndex();

                        switch (columnIndex) {
                        case 0:
                        	mediaOwner.setMediaOwner((String) getCellValue(nextCell));
                            break;
                        case 1:
                            double value = (Double) getCellValue(nextCell);
                            int value2 = (int) value;
                            mediaOwner.setPriority(value2);
                            break;
                        default:
                            break;
                        }
                    }
                    mediaOwnersList.add(mediaOwner);
                }
            }
            mediaOwnersAndPriorities.setMediaOwners(mediaOwnersList);
            LOG.debug("Parsed File and got the list",mediaOwnersAndPriorities.toString());
        } catch (FileNotFoundException e) {
            LOG.error("FileNotFoundException::" + e.getMessage());
        } catch (IOException e) {
            LOG.error("IOException::" + e.getMessage());
        }
        return mediaOwnersAndPriorities;
    }

   
    private MediaOwners parseUploadedCsvFile(MultipartFile uploadFile)
            throws IOException {
       
        List<CommercialInputStore> commInputList = new ArrayList<>();
        MediaOwnersDo mediaOwnersAndPriorities = new MediaOwnersDo();
    	MediaOwnerDo mediaOwner= null;
    	List<MediaOwner> mediaOwnersList = new ArrayList<>();
    	
        try (CSVReader reader = new CSVReader(new InputStreamReader(uploadFile.getInputStream()), FILE_SEPERATOR);) {

            List<String[]> records = reader.readAll();

            Iterator<String[]> iterator = records.iterator();
            // skip header row
            iterator.next();

            while (iterator.hasNext()) {
                String[] record = iterator.next();
                mediaOwner = new MediaOwnerDo();
                mediaOwner.setMediaOwner(record[0]);
                mediaOwner.setPriority(Integer.parseInt(record[1]));
                
                mediaOwnersList.add(mediaOwner);
            }
            
            mediaOwnersAndPriorities.setMediaOwners(mediaOwnersList);

        } catch (FileNotFoundException e) {
            LOG.error("FileNotFoundException::" + e.getMessage());
            throw e;
        } catch (IOException e) {
            throw e;
        }
        return mediaOwnersAndPriorities;
    }

    public String computeSpcAlgorithm(String commInputToJson, String traderInputToJson, String pythonScriptPath){
      	return PythonScriptExecutor.executeSPCRecommendationScript(commInputToJson, traderInputToJson, pythonScriptPath);
    }

    public MediaOwners parseUploadedFile(MultipartFile uploadedFile) throws Exception{
        MediaOwners mediaOwnersAndPriorities = null;
            if (uploadedFile.getSize() == 0) {
                LOG.info("file size is"+ uploadedFile.getSize());
                LOG.error("File is having no contents");
                throw new BadRequestException("Bad Request, Please upload Media Owner Priority file with valid contents.");
            } else {
                String fileType = uploadedFile.getOriginalFilename().split("\\.")[1];
                LOG.info("Media Owner Priority file type :: {}", fileType);

                if (fileType.equals("csv")) {
                	mediaOwnersAndPriorities = parseUploadedCsvFile(uploadedFile);

                } else if (fileType.equals("xlsx")) {
                	mediaOwnersAndPriorities = parseUploadedExcelFile(uploadedFile);
                } else {
                    LOG.error("Commercial Media Owner uploaded file type is not csv or excel " + uploadedFile.getOriginalFilename());
                    throw new BadRequestException("Bad Request, Please upload file with .xlsx extention");
                 }
            }
        return mediaOwnersAndPriorities;
    }

    public MediaOwners parseFileAndreturnCommercialInputsList(MultipartFile uploadFile, int profileId) throws Exception{
        return parseUploadedFile(uploadFile);
    }

    public boolean isCollectionEmpty(Collection<?> collection) {
        if (collection == null || collection.isEmpty()) {
            return true;
        }
        return false;
    }
    
    private Object getCellValue(Cell cell) {

        switch (cell.getCellTypeEnum()) {
        case STRING:
            return cell.getRichStringCellValue().getString();
        case BOOLEAN:
            return cell.getBooleanCellValue();
        case NUMERIC:
            return cell.getNumericCellValue();
        case BLANK:
            break;
        case ERROR:
            break;
        case FORMULA:
            break;
        case _NONE:
            break;
        default:
            break;
        }

        return FILE_SUCCESS_MESSAGE;
    }

}
