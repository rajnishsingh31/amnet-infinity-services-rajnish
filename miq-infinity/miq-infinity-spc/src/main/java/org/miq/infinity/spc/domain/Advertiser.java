package org.miq.infinity.spc.domain;

public interface Advertiser {
	int getAdvertiserId();
	String getAdvertiserName();
}
