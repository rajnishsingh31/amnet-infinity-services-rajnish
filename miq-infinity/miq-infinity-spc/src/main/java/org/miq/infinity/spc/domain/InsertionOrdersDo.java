package org.miq.infinity.spc.domain;

import java.util.List;

public class InsertionOrdersDo implements InsertionOrders {
	
	private List<InsertionOrder> insertionOrders;

	public List<InsertionOrder> getInsertionOrders() {
		return insertionOrders;
	}

	public void setInsertionOrders(List<InsertionOrder> insertionOrders) {
		this.insertionOrders = insertionOrders;
	}
	
	
	
}
