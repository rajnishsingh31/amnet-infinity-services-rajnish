package org.miq.infinity.spc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PythonScriptExecutor {
	private static final Logger LOG = LoggerFactory.getLogger(SpcUtil.class);
	
	//computeSpcAlgorithm
	public static String executeSPCRecommendationScript(String commInputToJson, String traderInputToJson, String pythonScriptPath) {
		LOG.info("Inside python script executor:For file: "+pythonScriptPath);
		LOG.debug("inside executeSPCRecommendationScript()");
		String spcComputationOutput = null;
		String spcCompute = null;

		String[] cmd = new String[4];
		cmd[0] = "python";
		cmd[1] = pythonScriptPath;
		cmd[2] = commInputToJson;
		cmd[3] = traderInputToJson;

		Runtime rt = Runtime.getRuntime();
		Process pr;
		try {
			pr = rt.exec(cmd);
			BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));

			while ((spcComputationOutput = bfr.readLine()) != null) {
				spcCompute = spcComputationOutput;
			}
		} catch (IOException e) {
			LOG.error("Exception while executing SPC python script :: {}", e);
		}
		LOG.info("SPC RECOMMENDATION OUTPUT AFTER executing script is "+spcCompute);
		LOG.debug("Exiting executeSPCRecommendationScript()");
		return spcCompute;
	}
	
	public static String executeScript(String[] args) {
		String executionOutput = null;
		String collectiveExecutionOutput = null;
		Runtime rt = Runtime.getRuntime();
		Process pr;
		try {
			pr = rt.exec(args);
			BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));

			while ((executionOutput = bfr.readLine()) != null) {
				collectiveExecutionOutput = executionOutput;
			}
		} catch (IOException e) {
			LOG.error("Exception in reading computing script :: {}", e);
		}
		System.out.println("Returning collectiveExecutionOutput:"+collectiveExecutionOutput);
		return collectiveExecutionOutput;
	}

}
