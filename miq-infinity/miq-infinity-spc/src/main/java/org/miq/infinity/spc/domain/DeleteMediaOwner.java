package org.miq.infinity.spc.domain;

public interface DeleteMediaOwner {
	    
	    public int getId();

	    public int getPriority();

	    public String getDomain();
}
