package org.miq.infinity.spc.redshiftmanager;

import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:redshift-db.properties")
public class RedshiftConnectionProvider {
	
	private static final String APPNEXUS_ADVERTISERS = "AppnexusAdvertisers";

	private static final String DBM_ADVERTISERS = "DbmAdvertisers";

	private static final String MEDIA_OWNER_PRIORITIES = "MediaOwnerPriorities";

	private static final Logger LOG = LoggerFactory.getLogger(RedshiftConnectionProvider.class);
	
	@Value("${amnetreports.datasource.url}")
	private  String amnetreportsConnectionURL;
	
	@Value("${amnetreports.datasource.username}")
	private  String amnetreportsUsername;
	
	@Value("${amnetreports.datasource.password}")
	private  String amnetreportsPassword;
	
	@Value("${amnetdata.datasource.url}")
	private  String amnetDataConnectionURL;
	
	@Value("${amnetdata.datasource.username}")
	private  String amnetDataUsername;
	
	@Value("${amnetdata.datasource.password}")
	private  String amnetDataPassword;
	
	private  Connection connection = null;
	
	public String getAmnetreportsConnectionURL() {
		return amnetreportsConnectionURL;
	}

	public void setAmnetreportsConnectionURL(String amnetreportsConnectionURL) {
		this.amnetreportsConnectionURL = amnetreportsConnectionURL;
	}

	public String getAmnetreportsUsername() {
		return amnetreportsUsername;
	}

	public void setAmnetreportsUsername(String amnetreportsUsername) {
		this.amnetreportsUsername = amnetreportsUsername;
	}

	public String getAmnetreportsPassword() {
		return amnetreportsPassword;
	}

	public void setAmnetreportsPassword(String amnetreportsPassword) {
		this.amnetreportsPassword = amnetreportsPassword;
	}

	public String getAmnetDataConnectionURL() {
		return amnetDataConnectionURL;
	}

	public void setAmnetDataConnectionURL(String amnetDataConnectionURL) {
		this.amnetDataConnectionURL = amnetDataConnectionURL;
	}

	public String getAmnetDataUsername() {
		return amnetDataUsername;
	}

	public void setAmnetDataUsername(String amnetDataUsername) {
		this.amnetDataUsername = amnetDataUsername;
	}

	public String getAmnetDataPassword() {
		return amnetDataPassword;
	}

	public void setAmnetDataPassword(String amnetDataPassword) {
		this.amnetDataPassword = amnetDataPassword;
	}
	
	
	public  Connection getRedShiftConnection(String type) {
		LOG.info("Establishing Redshift Connection required for getting {}" , type);
		if(type == MEDIA_OWNER_PRIORITIES) {
			connection = JDBCConnectionManager.getConnection(amnetDataConnectionURL, amnetDataUsername, amnetDataPassword);
		}else if(type.equals(DBM_ADVERTISERS) || type.equals(APPNEXUS_ADVERTISERS)) {
			connection = JDBCConnectionManager.getConnection(amnetreportsConnectionURL, amnetreportsUsername, amnetreportsPassword);
		}
		return connection;
	}
	
}
