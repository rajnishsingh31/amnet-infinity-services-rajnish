package org.miq.infinity.spc.domain;

import java.util.Date;
import java.util.List;

public interface TraderInputCriteria {

    public String getProfileName();

    public int getAdvertiserId();
    
    public int getInsertionOrderId();
    
    public int getCampaignId();

    public Date getStartdate();

    public Date getEnddate();

    public long getImpressions();

    public long getBudget();

    public String getCtrGoal();

    public String getViewabilityGoal();

   // public List<String> getDeviceType();
    public String getDeviceType();

    public List<String> getCreativeSize();

}
