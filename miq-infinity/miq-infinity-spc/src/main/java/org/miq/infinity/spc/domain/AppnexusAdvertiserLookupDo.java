package org.miq.infinity.spc.domain;

public class AppnexusAdvertiserLookupDo implements AppnexusAdvertiserLookup{
	
	private long advertiserId;
	private long ioId;
	private long lineItemId;
	private long placementId;
	private String advertiserName;
	private String ioName;
	private String lineItemName;
	private String placementName;
	
	
	public long getAdvertiserId() {
		return advertiserId;
	}
	public void setAdvertiserId(long advertiserId) {
		this.advertiserId = advertiserId;
	}
	public long getIoId() {
		return ioId;
	}
	public void setIoId(long ioId) {
		this.ioId = ioId;
	}
	public long getLineItemId() {
		return lineItemId;
	}
	public void setLineItemId(long lineItemId) {
		this.lineItemId = lineItemId;
	}
	public long getPlacementId() {
		return placementId;
	}
	public void setPlacementId(long placementId) {
		this.placementId = placementId;
	}
	public String getAdvertiserName() {
		return advertiserName;
	}
	public void setAdvertiserName(String advertiserName) {
		this.advertiserName = advertiserName;
	}
	public String getIoName() {
		return ioName;
	}
	public void setIoName(String ioName) {
		this.ioName = ioName;
	}
	public String getLineItemName() {
		return lineItemName;
	}
	public void setLineItemName(String lineItemName) {
		this.lineItemName = lineItemName;
	}
	public String getPlacementName() {
		return placementName;
	}
	public void setPlacementName(String placementName) {
		this.placementName = placementName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (advertiserId ^ (advertiserId >>> 32));
		result = prime * result + ((advertiserName == null) ? 0 : advertiserName.hashCode());
		result = prime * result + (int) (ioId ^ (ioId >>> 32));
		result = prime * result + ((ioName == null) ? 0 : ioName.hashCode());
		result = prime * result + (int) (lineItemId ^ (lineItemId >>> 32));
		result = prime * result + ((lineItemName == null) ? 0 : lineItemName.hashCode());
		result = prime * result + (int) (placementId ^ (placementId >>> 32));
		result = prime * result + ((placementName == null) ? 0 : placementName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppnexusAdvertiserLookupDo other = (AppnexusAdvertiserLookupDo) obj;
		if (advertiserId != other.advertiserId)
			return false;
		if (advertiserName == null) {
			if (other.advertiserName != null)
				return false;
		} else if (!advertiserName.equals(other.advertiserName))
			return false;
		if (ioId != other.ioId)
			return false;
		if (ioName == null) {
			if (other.ioName != null)
				return false;
		} else if (!ioName.equals(other.ioName))
			return false;
		if (lineItemId != other.lineItemId)
			return false;
		if (lineItemName == null) {
			if (other.lineItemName != null)
				return false;
		} else if (!lineItemName.equals(other.lineItemName))
			return false;
		if (placementId != other.placementId)
			return false;
		if (placementName == null) {
			if (other.placementName != null)
				return false;
		} else if (!placementName.equals(other.placementName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "AppnexusAdvertiserLookupDo [advertiserId=" + advertiserId + ", ioId=" + ioId + ", lineItemId="
				+ lineItemId + ", placementId=" + placementId + ", advertiserName=" + advertiserName + ", ioName="
				+ ioName + ", lineItemName=" + lineItemName + ", placementName=" + placementName + "]";
	}
	
	
}
