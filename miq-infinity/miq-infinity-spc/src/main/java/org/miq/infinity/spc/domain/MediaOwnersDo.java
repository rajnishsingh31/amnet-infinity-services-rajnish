package org.miq.infinity.spc.domain;

import java.util.List;

public class MediaOwnersDo implements MediaOwners{
	
	private List<MediaOwner> mediaOwners;

	public List<MediaOwner> getMediaOwners() {
		return mediaOwners;
	}

	public void setMediaOwners(List<MediaOwner> mediaOwners) {
		this.mediaOwners = mediaOwners;
	}
	
	
	public void add(MediaOwner mediaOwner) {
		this.mediaOwners.add(mediaOwner);
	}

	@Override
	public String toString() {
		return "MediaOwnersDo [mediaOwners=" + mediaOwners + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mediaOwners == null) ? 0 : mediaOwners.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediaOwnersDo other = (MediaOwnersDo) obj;
		if (mediaOwners == null) {
			if (other.mediaOwners != null)
				return false;
		} else if (!mediaOwners.equals(other.mediaOwners))
			return false;
		return true;
	}
	
	
 	
}