package org.miq.infinity.spc.domain;

public class MediaOwnerDo implements MediaOwner{
	
	private String mediaOwner;
	private int priority;
	public String getMediaOwner() {
		return mediaOwner;
	}
	public void setMediaOwner(String mediaOwner) {
		this.mediaOwner = mediaOwner;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mediaOwner == null) ? 0 : mediaOwner.hashCode());
		result = prime * result + priority;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediaOwnerDo other = (MediaOwnerDo) obj;
		if (mediaOwner == null) {
			if (other.mediaOwner != null)
				return false;
		} else if (!mediaOwner.equals(other.mediaOwner))
			return false;
		if (priority != other.priority)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "MediaOwnersDo [mediaOwner=" + mediaOwner + ", priority=" + priority + "]";
	}
	
}