package org.miq.infinity.spc.repository;

import java.sql.SQLException;
import java.util.List;

import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.domain.DeleteMediaOwner;
import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwners;
import org.miq.infinity.spc.entity.CommercialInputStore;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface CommercialMediaOwnersRepository {

    public String updateCommercialRecord(CommInputCreteriaImpl commInputCriteria) throws JsonProcessingException;

    public DeleteMediaOwner deleteMediaOwner(int id);

    public MediaOwners saveMediaOwnersAndAssociatedPriorities(MediaOwners mediaOwnersAndAssociatedPrioritiesList,int profileId);

    public List<CommercialInputStore> getMediaOwnersAndAssociatedPriorities();
    
    public List<MediaOwner> getDbmMediaOwnersFromRedshiftLookup() throws SQLException;
    
   
}
