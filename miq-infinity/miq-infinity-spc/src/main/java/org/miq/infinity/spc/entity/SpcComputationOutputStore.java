package org.miq.infinity.spc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "spc_computation_output_store")
public class SpcComputationOutputStore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "profile_id")
    private int profileId;
    
    @Column(name = "advertiser_id")
    private int advertiserId;
    
    @Column(name = "insertionorder_id")
    private int insertionOrderId;
   
    @Column(name = "campaign_id")
    private int campaignId;
    
    @Column(name = "domain")
    private String domain;

    @Column(name = "recommended_cpm")
    private double recommendedCpm;

    @Column(name = "expected_ctr")
    private double expectedCtr;

    @Column(name = "expected_viewability")
    private String expectedViewability;

    @Column(name = "expected_impressions")
    private long expectedImpressions;

    @Column(name = "recommendation_identifier")
    private int recommendationIdentifier;

    @Column(name = "campaign_type")
    private String campaignType;

    @Column(name = "creation_date")
    // @Temporal(TemporalType.DATE)
    private Date creationDate;

    @Column(name = "is_active")
    private int isActive;

    @Column(name = "device_type")
    private String deviceType;

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }
    
    
    public int getAdvertiserId() {
		return advertiserId;
	}

	public void setAdvertiserId(int advertiserId) {
		this.advertiserId = advertiserId;
	}

	 

	public int getInsertionOrderId() {
		return insertionOrderId;
	}

	public void setInsertionOrderId(int insertionOrderId) {
		this.insertionOrderId = insertionOrderId;
	}

	public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public double getRecommendedCpm() {
        return recommendedCpm;
    }

    public void setRecommendedCpm(double recommendedCpm) {
        this.recommendedCpm = recommendedCpm;
    }

    public double getExpectedCtr() {
        return expectedCtr;
    }

    public void setExpectedCtr(double expectedCtr) {
        this.expectedCtr = expectedCtr;
    }

    public String getExpectedViewability() {
        return expectedViewability;
    }

    public void setExpectedViewability(String expectedViewability) {
        this.expectedViewability = expectedViewability;
    }

    public long getExpectedImpressions() {
        return expectedImpressions;
    }

    public void setExpectedImpressions(long expectedImpressions) {
        this.expectedImpressions = expectedImpressions;
    }

    public int getRecommendationIdentifier() {
        return recommendationIdentifier;
    }

    public void setRecommendationIdentifier(int recommendationIdentifier) {
        this.recommendationIdentifier = recommendationIdentifier;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

	@Override
	public String toString() {
		return "SpcComputationOutputStore [id=" + id + ", profileId=" + profileId + ", advertiserId=" + advertiserId
				+ ", insertionOrderId=" + insertionOrderId + ", campaignId=" + campaignId + ", domain=" + domain
				+ ", recommendedCpm=" + recommendedCpm + ", expectedCtr=" + expectedCtr + ", expectedViewability="
				+ expectedViewability + ", expectedImpressions=" + expectedImpressions + ", recommendationIdentifier="
				+ recommendationIdentifier + ", campaignType=" + campaignType + ", creationDate=" + creationDate
				+ ", isActive=" + isActive + ", deviceType=" + deviceType + "]";
	}

    

}
