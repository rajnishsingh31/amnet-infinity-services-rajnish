/*
 * Author : Subhan Ansari
 */

package com.miq.infinity.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.miq.infinity.engine.LookupService;
import com.miq.infinity.entity.LookupStore;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@RestController
@RequestMapping(RestDispatcher.SERVICE_VERSION_1)
@CrossOrigin
@AllArgsConstructor
@NoArgsConstructor
public class LookupController  extends RestDispatcher {

	@Autowired
	private LookupService lookupService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LookupController.class);
	
	public static class Uri {
		private Uri() {}
		public static final String LOOKUPS = "/lookups";
		public static final String LOOKUPS_LIST = LOOKUPS + "/list";
	}
	
	@ApiOperation(value = "Return Lookups list")
	@RequestMapping(value = Uri.LOOKUPS_LIST , method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<LookupStore>> getAllLookups()  {
		LOGGER.info("Getting all lookups");
		long startTime= System.currentTimeMillis();
		ResponseEntity<List<LookupStore>> responseEntity= new ResponseEntity<>(lookupService.getAllLookups(),HttpStatus.OK);
		long timeTakenByAPI = System.currentTimeMillis()-startTime;
		LOGGER.info("Exiting LookupController: getAllLookups after " ,(timeTakenByAPI) ," milliseconds."); 
		return responseEntity;
	} 
	
}
