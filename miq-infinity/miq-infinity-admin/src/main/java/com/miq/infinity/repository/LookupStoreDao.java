/*
 * Author : Subhan Ansari
 */

package com.miq.infinity.repository;

import org.springframework.data.repository.CrudRepository;

import com.miq.infinity.entity.LookupStore;

public interface LookupStoreDao extends CrudRepository<LookupStore, Long>{
	
	public LookupStore findByName(String name);
}
