package org.miq.infinity.spc.domain;

public class AdvertiserDo implements Advertiser{
	private int advertiserId;
	private String advertiserName;
	
	public int getAdvertiserId() {
		return advertiserId;
	}
	public void setAdvertiserId(int advertiserId) {
		this.advertiserId = advertiserId;
	}
	public String getAdvertiserName() {
		return advertiserName;
	}
	public void setAdvertiserName(String advertiserName) {
		this.advertiserName = advertiserName;
	}
}
