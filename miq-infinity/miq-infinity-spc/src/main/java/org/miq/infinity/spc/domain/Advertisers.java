package org.miq.infinity.spc.domain;

import java.util.List;

public interface Advertisers {
	List<Advertiser> getAdvertisers();
}