package org.miq.infinity.spc.domain;

public interface DbmAdvertiserLookup {
	
	public long getId();
	public long getIoId();
	public long getAdvertiserId();
	public String getName();
	public String getIoName();
	public String getAdvertiserName();
	
}
