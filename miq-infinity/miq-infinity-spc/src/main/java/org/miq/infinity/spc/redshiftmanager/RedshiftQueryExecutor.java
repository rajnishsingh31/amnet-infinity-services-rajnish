package org.miq.infinity.spc.redshiftmanager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedshiftQueryExecutor {
	private static final Logger LOG = LoggerFactory.getLogger(RedshiftQueryExecutor.class);
	
	public static ResultSet executeQuery(Connection connection, String query) throws SQLException{
		ResultSet resultSet = null;
		Statement statement = null;
		try {
			statement = connection.createStatement();
			LOG.info("Executing query:{} with statement {}",query,statement);
			resultSet = statement.executeQuery(query);
			LOG.info("Got Result after executing query");
			return resultSet;
	}catch(SQLException sqlException) {
		LOG.error("Exception occured while executing query",sqlException.getMessage(),sqlException);
	}
	return resultSet;
	}
}
