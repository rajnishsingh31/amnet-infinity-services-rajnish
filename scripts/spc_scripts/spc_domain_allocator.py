import pandas as ps
import json
import sys
import numpy as np
import psycopg2

#### parameter string 1 - Commercial input 
#### parameter string 2 - traders input

## Connecting AmnetData for historical guarantee
conn_string = "dbname='amnetdata' port='5439' user='reports' password='KAH_97sjk?32' host='amnetfeeds.cqk3xmolxdry.us-east-1.redshift.amazonaws.com'"
conn = psycopg2.connect(conn_string)
cur = conn.cursor()

q = """select * from shamshu_spc_hg_output_1"""
cur.execute(q)
res = cur.fetchall()
conn.close()
res = list(res)
hg = ps.DataFrame(res)
hg.columns = ['site_domain','device_type','tot_imp','ctr_percentage','cpm','viewability']


commercial = sys.argv[1]
trader = sys.argv[2]

##commercial = """[
##  {
##    "id": 1,
##    "profileId": 8,
##    "domain": "mail.aol.com",
##    "priority": 1,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 2,
##    "profileId": 8,
##    "domain": "uk.yahoo.com",
##    "priority": 2,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 3,
##    "profileId": 8,
##    "domain": "cityam.com",
##    "priority": 3,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 4,
##    "profileId": 8,
##    "domain": "lastminute.com",
##    "priority": 4,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 5,
##    "profileId": 8,
##    "domain": "youtube.com",
##    "priority": 5,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 6,
##    "profileId": 8,
##    "domain": "dailymail.co.uk",
##    "priority": 6,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 7,
##    "profileId": 8,
##    "domain": "telegraph.co.uk",
##    "priority": 7,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 8,
##    "profileId": 8,
##    "domain": "ebay.co.uk",
##    "priority": 8,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 9,
##    "profileId": 8,
##    "domain": "gumtree.com",
##    "priority": 9,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 10,
##    "profileId": 8,
##    "domain": "condenast.com",
##    "priority": 10,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 11,
##    "profileId": 8,
##    "domain": "theguardian.com",
##    "priority": 11,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 12,
##    "profileId": 8,
##    "domain": "timeinc.com",
##    "priority": 12,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 13,
##    "profileId": 8,
##    "domain": "mashable.com",
##    "priority": 14,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 14,
##    "profileId": 8,
##    "domain": "news.sky.com",
##    "priority": 15,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 15,
##    "profileId": 8,
##    "domain": "trinitymirror.com",
##    "priority": 16,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 16,
##    "profileId": 8,
##    "domain": "netmums.com",
##    "priority": 17,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 17,
##    "profileId": 8,
##    "domain": "mumsnet.com",
##    "priority": 18,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 18,
##    "profileId": 8,
##    "domain": "dennis.co.uk",
##    "priority": 19,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 19,
##    "profileId": 8,
##    "domain": "haymarket.com",
##    "priority": 20,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 20,
##    "profileId": 8,
##    "domain": "autotrader.co.uk",
##    "priority": 21,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 21,
##    "profileId": 8,
##    "domain": "bauermedia.co.uk",
##    "priority": 22,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 22,
##    "profileId": 8,
##    "domain": "news.co.uk",
##    "priority": 23,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 23,
##    "profileId": 8,
##    "domain": "talksport.com",
##    "priority": 24,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 24,
##    "profileId": 8,
##    "domain": "hearst.com",
##    "priority": 25,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 25,
##    "profileId": 8,
##    "domain": "northernandshell.co.uk",
##    "priority": 26,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 26,
##    "profileId": 8,
##    "domain": "esimedia.co.uk",
##    "priority": 27,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 27,
##    "profileId": 8,
##    "domain": "immediate.co.uk",
##    "priority": 29,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 28,
##    "profileId": 8,
##    "domain": "timeout.com",
##    "priority": 30,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  },
##  {
##    "id": 29,
##    "profileId": 8,
##    "domain": "bloomberg.com",
##    "priority": 31,
##    "createDate": "2018-01-16T03:55:37.000+0000",
##    "updateDate": "2018-01-16T03:55:37.000+0000",
##    "isActive": 1
##  }
##]"""
##
##trader = """{"profileName":"sunil.com","advertiserId":87890,"campaignId":123,"campaignType":"rtg","startdate":1516774107138,"enddate":1516774107138,"impressions":9000,"budget":4524,"ctrGoal":"0.03","viewabilityGoal":">10%","deviceType":"Desktop","creativeSize":["300x500"]}"""


#Converting Commercial input json as dataframe
comm = json.loads(commercial)
priority_dict = dict()
for i in range(len(comm)):
        this_obj = comm[i]
        this_domain = this_obj['domain']
        this_priority = this_obj['priority']
        priority_dict[this_domain] = this_priority

### Inventory Availability for creative width and height match
##cs = objj['creativeSize'].split('x')
##width = cs[0]
##height = cs[1]
####print(width)
####print(height)
##
##VR = VR.replace('>','')
##VR = VR.replace('%','')
##print(VR)

##q = """select distinct app_url from dbm_iar_report_new 
##where creative_width ="""+str(width)+""" and creative_height = """+str(height)+"""
##and cast(split_part(split_part(active_view_expected_viewability,'=',2),'%',1) as integer) >= """+str(VR)

 
################################ Allocation Algorithm ########################################
### Rank to Weights
def ranksToWeights(a,max_rank):
        deno = 0
        for p in a:
                den = max_rank-p+1
                deno += den
        weights = []
        for p in a:
                wt = float(max_rank-p+1)/deno
                weights.append(wt)
        return weights

##def generate_spo(commercial,trader):
##        comm_obj = json.loads(commercial)
##        tra_obj  = json.loads(trader)
##
##        df = ps.DataFrame(columns = ('domain','recommendedCpm','expectedCtr','expectedViewability','expectedImpressions'))
##
##        for i in range(len(comm_obj)):
##                this_obj = comm_obj[i]
##                this_domain = this_obj['domain']
##                this_imp = np.random.randint(0,10000)
##                this_ctr = np.random.uniform(0,0.04)
##                this_cpm = np.random.randint(0,15)
##                this_vb = np.random.randint(0,100)
##                df.loc[i] = (this_domain,this_cpm,this_ctr,this_vb,this_imp)
##
##        out_stri = df.to_json(orient = 'records')
##        return out_stri
##
##out = generate_spo(comm,trad)
##print(out)

# campaign request
objj = json.loads(trader)
this_camp_id = objj['campaignId']
this_client_id = objj['advertiserId']
this_line = objj['campaignType']
this_NOI = int(objj['impressions'])
this_device = str(objj['deviceType'])
this_creative_size = str(objj['creativeSize'])
budget = int(objj['budget'])
this_CTR = str(objj['ctrGoal'])
this_CTR = float(this_CTR.replace('%',''))
this_CPM = (float(budget)/this_NOI)*1000
this_VR  = str(objj['viewabilityGoal'])
this_VR = this_VR.replace('>','')
this_VR = this_VR.replace('%','')
this_VR = float(this_VR)/100

##### filtering conditions
out1 = hg[(hg['viewability'] >= this_VR)]
out2 = hg[(hg['ctr_percentage'] >= this_CTR)]
out3 = hg[(hg['cpm'] >= this_CPM)]

dom1 = list(out1['site_domain'])
dom2 = list(out2['site_domain'])
dom3 = list(out3['site_domain'])
domains = list(set(dom1+dom2+dom3))

prios = list()
avail_domains = list()
for d in domains:
    if(priority_dict.has_key(d)):
        avail_domains.append(d)
        prio = priority_dict[d]
        prios.append(prio)
weights = ranksToWeights(prios,30)
out_df = ps.DataFrame(columns = ('domain','deviceType','recommendedCpm','expectedCtr','expectedViewability','expectedImpressions'))

cnt = 0
for d in range(len(avail_domains)):
    this_domain = avail_domains[d]
    if(priority_dict.has_key(this_domain)):
        share_imp = weights[d]*(this_NOI+0.10*this_NOI)
        get_rec = hg[(hg['site_domain']== this_domain)]
        ctr_perc = list(get_rec['ctr_percentage'])
        cpm = list(get_rec['cpm'])
        dev_type = list(get_rec['device_type'])
        viewb = list(get_rec['viewability'])
        out_df.loc[cnt] = (this_domain,this_device,cpm[0],ctr_perc[0],viewb[0],long(share_imp))
        cnt += 1


out_stri = out_df.to_json(orient= 'records')
print(out_stri)
################################ Allocation Algorithm ########################################

    
