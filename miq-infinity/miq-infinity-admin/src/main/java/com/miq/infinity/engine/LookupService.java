/*
 * Author : Subhan Ansari
 */
package com.miq.infinity.engine;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.miq.infinity.entity.LookupStore;
import com.miq.infinity.repository.LookupStoreDao;

@Component
public class LookupService {
	
	@Autowired
	private LookupStoreDao lookupStoreDao;
	
	public List<LookupStore> getAllLookups()    {
		
    	return (List<LookupStore>) lookupStoreDao.findAll();
	} 
}
