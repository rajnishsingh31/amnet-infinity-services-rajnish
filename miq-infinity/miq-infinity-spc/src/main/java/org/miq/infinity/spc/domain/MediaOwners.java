package org.miq.infinity.spc.domain;

import java.util.List;

public interface MediaOwners {
	List<MediaOwner> getMediaOwners();
}
