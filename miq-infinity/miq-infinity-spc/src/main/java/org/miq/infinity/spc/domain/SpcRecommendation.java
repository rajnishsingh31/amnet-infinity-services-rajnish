package org.miq.infinity.spc.domain;

public interface SpcRecommendation {
    public String getDomain();

    public double getRecommendedCpm();

    public double getExpectedCtr();

    public String getExpectedViewability();

    public long getExpectedImpressions();

    public String getDeviceType();
}
