package org.miq.infinity.spc.repository;

import org.miq.infinity.spc.entity.TraderInputStore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TraderInputStoreDao extends JpaRepository<TraderInputStore, Long> {

}
