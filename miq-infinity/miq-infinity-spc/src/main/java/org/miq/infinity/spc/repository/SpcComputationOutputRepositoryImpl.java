package org.miq.infinity.spc.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.miq.infinity.spc.domain.SpcGeneratedRecommendationsDo;
import org.miq.infinity.spc.domain.SpcRecommendationDo;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.exception.DataBaseAccessException;
import org.miq.infinity.spc.util.SpcUtil;
import org.python.jline.internal.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Repository
public class SpcComputationOutputRepositoryImpl implements SpcComputationOutputRepository {
    
    
    private static final Logger LOG = LoggerFactory.getLogger(SpcComputationOutputRepositoryImpl.class);

    @Autowired
    private SpcComputationOutputDao spcComputationOutputDao;

    @Autowired
    private SpcUtil spcUtil;

    private static final String DATA_SUCCESS = "SUCCESS";

    @Override
    public String getRecommendationIdentifier() {
        return spcComputationOutputDao.findMaxRecommendationIdentifier();
    }

    @Override 
    public void updateSpcOutput(int isActive) {

        List<SpcComputationOutputStore> spcList = spcComputationOutputDao.findAll();
        
        Iterator<SpcComputationOutputStore> iterator = spcList.iterator();

        while (iterator.hasNext()) {
            SpcComputationOutputStore spcoutputstore = iterator.next();

            if (spcoutputstore.getIsActive() != 0) {
                spcoutputstore.setIsActive(isActive);
            }
        }
        String spcUpdatedList=spcList.toString();
        LOG.info("Updated spc output :: {} ",spcUpdatedList);
        spcComputationOutputDao.save(spcList);
    }
 
    @Override
    public List<SpcComputationOutputStore> getAllSpcOutputList(int isActive) {
        List<SpcComputationOutputStore> spcComputationOutputStoreList = spcComputationOutputDao.findByIsActive(isActive);
       
        if(spcUtil.isCollectionEmpty(spcComputationOutputStoreList))
        {
            LOG.error("Listing spc output :: {}",spcComputationOutputStoreList);
            throw new DataBaseAccessException("Internal Database Server Error");
        }
        
        return spcComputationOutputStoreList;

    }

    @Override
    public List<SpcRecommendationDo> saveSpcOutput(String spcOutput, int profileId, int isActive,
            TraderInputCriteriaImpl traderinputcriteria, int updatedRecommendationId) {
    	
    	List<SpcRecommendationDo> spcComputationoutput =null;
        isActive = isActive + 1;

        Gson gson = new Gson();
        Date creationDate = new Date();

        List<SpcComputationOutputStore> spccomputationoutputlist = new ArrayList<>();

        try {
                spcComputationoutput = gson.fromJson(spcOutput,
                new TypeToken<List<SpcRecommendationDo>>() {
                }.getType());

        Iterator<SpcRecommendationDo> iterator = spcComputationoutput.iterator();

        while (iterator.hasNext()) {
            SpcComputationOutputStore spcoutputstore = new SpcComputationOutputStore();
            SpcRecommendationDo spcComputationcriteria = iterator.next();
            
            spcoutputstore.setAdvertiserId(traderinputcriteria.getAdvertiserId());
            spcoutputstore.setInsertionOrderId(traderinputcriteria.getInsertionOrderId());
            spcoutputstore.setDomain(spcComputationcriteria.getDomain());
            spcoutputstore.setExpectedCtr(spcComputationcriteria.getExpectedCtr());
            spcoutputstore.setExpectedImpressions(spcComputationcriteria.getExpectedImpressions());
            spcoutputstore.setExpectedViewability(spcComputationcriteria.getExpectedViewability());
            spcoutputstore.setRecommendationIdentifier(updatedRecommendationId);
            spcoutputstore.setRecommendedCpm(spcComputationcriteria.getRecommendedCpm());
            spcoutputstore.setDeviceType(spcComputationcriteria.getDeviceType());
            spcoutputstore.setProfileId(profileId);
            spcoutputstore.setCampaignId(traderinputcriteria.getCampaignId());
            spcoutputstore.setCampaignType(traderinputcriteria.getCampaignType());
            spcoutputstore.setCreationDate(creationDate);
            spcoutputstore.setIsActive(isActive);

            spccomputationoutputlist.add(spcoutputstore);
        }
        }
        catch(Exception e)
        {
            Log.error("Error in Parsing SPC computation output",e.getMessage());
        }
        
        if (spcUtil.isCollectionEmpty(spccomputationoutputlist)) {
            throw new DataBaseAccessException("Internal Database Server Error, Problem in saving spc output");
        }
        spcComputationOutputDao.save(spccomputationoutputlist);
        return spcComputationoutput;
    }

}
