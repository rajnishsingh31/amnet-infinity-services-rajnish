package org.miq.infinity.spc.domain;

public class CampaignDo implements Campaign{
	
	private int campaignId;
	private String campaignName;
	
	public int getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
}
