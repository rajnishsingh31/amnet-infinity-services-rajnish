package org.miq.infinity.spc.engine.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.repository.ProfileInputRepository;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class SpcProfileServiceTest {
    
    @Mock
    ProfileInputRepository profileInputRepository;
    
    @InjectMocks
    SpcProfileService spcProfileService;
    
    
    @Test
    public void getProfileIdTest() {
        
        String profileName = "sunil.com";
        int profileId =1;
        
        Mockito.when(profileInputRepository.getAndSaveProfile(profileName)).thenReturn(profileId);
        
        int profileIdNew = spcProfileService.getProfileId(profileName);
        
        org.junit.Assert.assertEquals(profileIdNew, profileId);
    }
    
    
    

}
