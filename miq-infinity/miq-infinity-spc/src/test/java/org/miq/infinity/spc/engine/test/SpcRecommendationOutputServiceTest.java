package org.miq.infinity.spc.engine.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwnerDo;
import org.miq.infinity.spc.domain.MediaOwnersDo;
import org.miq.infinity.spc.domain.SPCRecommendationsAndTradersInputDo;
import org.miq.infinity.spc.domain.SpcRecommendationDo;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.CommercialMediaOwnersService;
import org.miq.infinity.spc.engine.SpcProfileService;
import org.miq.infinity.spc.engine.SpcRecommendationOutputService;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.exception.ResourceNotFoundException;
import org.miq.infinity.spc.repository.CommercialMediaOwnersRepository;
import org.miq.infinity.spc.repository.SpcComputationOutputRepository;
import org.miq.infinity.spc.util.SpcUtil;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration
public class SpcRecommendationOutputServiceTest {

    @Mock
    private SpcProfileService spcProfileService;

    @Mock
    private SpcUtil spcUtil;

    @Mock
    CommercialMediaOwnersService spcCommercialService;

    @Mock
    Environment environment;

    @Mock
    SpcComputationOutputRepository spcComputationOutputRepository;

    @InjectMocks
    SpcRecommendationOutputService spcRecommendationOutputService;
    
    @Mock
    CommercialMediaOwnersRepository commercialMediaOwnersRepository;

    TraderInputCriteriaImpl traderInput;

    List<CommercialInputStore> commList;
    
    List<MediaOwner> mediaOwnerList= null;
    MediaOwnersDo mediaOwnersDo = null;
    @Before
    public void init() {
        Date startDate = new Date();

        List<String> creativeSize = new ArrayList<>();
        creativeSize.add("300*500");
        List<String> deviceType = new ArrayList<>();
        deviceType.add("Desktop");
        
        traderInput = new TraderInputCriteriaImpl();
        traderInput.setAdvertiserId(1234);
        traderInput.setBudget(9000);
        traderInput.setCampaignId(4567);
        traderInput.setCampaignType("Retargetting");
        traderInput.setCtrGoal("0.02");
        traderInput.setCreativeSize(creativeSize);
      //  traderInput.setDeviceType(deviceType);
        traderInput.setImpressions(900000);
        traderInput.setStartdate(startDate);
        traderInput.setEnddate(startDate);

        commList = new ArrayList<CommercialInputStore>();

        CommercialInputStore commInput = new CommercialInputStore();

        Date createDate = new Date();
        commInput.setMediaOwner("Test.com");
        commInput.setPriority(1);
        commInput.setProfileId(10);
        commInput.setCreateDate(createDate);
        commInput.setUpdateDate(createDate);
        commInput.setIsActive(1);

        commList.add(commInput);
        
        mediaOwnerList = new ArrayList<>();
        MediaOwnerDo mediaOwnerDo = new MediaOwnerDo();
        mediaOwnerDo.setMediaOwner("Yahoo");
        mediaOwnerDo.setPriority(1);
        mediaOwnerList.add(mediaOwnerDo);
          
        mediaOwnersDo =new MediaOwnersDo();
        mediaOwnersDo.setMediaOwners(mediaOwnerList);

    }

    @Test
    public void computeTraderInputTest() throws JsonProcessingException{
        int profileId = 1;
        List<SpcRecommendationDo> tempList = new ArrayList<>();
        String scriptPath = "/home/sunil/test.py";

        ObjectMapper objectMapper = new ObjectMapper();

        String commInputToJson = objectMapper.writeValueAsString(commList);
        String traderInputToJson = objectMapper.writeValueAsString(traderInput);

        String spcOutput = "SPC_OUTPUT";
        String status= "SUCCESS";
        SPCRecommendationsAndTradersInputDo sPCRecommendationsAndTradersInputDo = new SPCRecommendationsAndTradersInputDo();
       Mockito.when(environment.getProperty("commercial.python.script.path")).thenReturn(scriptPath);

        Mockito.when(spcCommercialService.getMediaOwnersAndAssociatedPriorities()).thenReturn(commList);

        Mockito.when(spcUtil.computeSpcAlgorithm(commInputToJson, traderInputToJson, scriptPath)).thenReturn(spcOutput);

       Mockito.when(spcRecommendationOutputService.parseAndSaveSpcOutput(spcOutput, profileId, traderInput)).thenReturn(tempList);
        
        SPCRecommendationsAndTradersInputDo Computestatus = spcRecommendationOutputService.generateSPCRecommendations(traderInput, profileId);

        org.junit.Assert.assertEquals(status, Computestatus);

    }
     
    @Test(expected = ResourceNotFoundException.class)
    public void computeTraderInputNotFoundExceptionTest() throws JsonProcessingException{
        int profileId = 1;

        String scriptPath = "/home/sunil/test.py";
        
        List<CommercialInputStore> commList1 = null;
        
        Mockito.when(environment.getProperty("script.path")).thenReturn(scriptPath);

        Mockito.when(spcCommercialService.getMediaOwnersAndAssociatedPriorities()).thenReturn(commList);
        
        Mockito.when(spcUtil.isCollectionEmpty(commList1)).thenThrow(new ResourceNotFoundException("Commercial Input not found, Please upload file first"));

        spcRecommendationOutputService.generateSPCRecommendations(traderInput, profileId);
    } 
 
    @Test(expected = ResourceNotFoundException.class)
    public void computeTraderInputExceptionTest() throws IOException{
        int profileId = 1;

        String scriptPath = "/home/sunil/test.py";

        ObjectMapper objectMapper = new ObjectMapper();

        String commInputToJson = objectMapper.writeValueAsString(commList);
        String traderInputToJson = objectMapper.writeValueAsString(traderInput);

        Mockito.when(environment.getProperty("script.path")).thenReturn(scriptPath);

        Mockito.when(spcCommercialService.getMediaOwnersAndAssociatedPriorities()).thenReturn(commList);

        Mockito.when(spcUtil.computeSpcAlgorithm(commInputToJson, traderInputToJson, scriptPath))
                .thenReturn(null);
        
        spcRecommendationOutputService.generateSPCRecommendations(traderInput, profileId);
    }     
    @Test
    public void parseSpcOutputTest() {
        String recommendationId = "1";
        int updatedRecommendationId = Integer.parseInt(recommendationId);
        String spcOutput = "SPC_OUTPUT";
        int profileId = 1;
        int isActive = 1;
        List<SpcRecommendationDo> status = new ArrayList<>();
        Mockito.when(spcComputationOutputRepository.getRecommendationIdentifier()).thenReturn(recommendationId);
        Mockito.when(spcComputationOutputRepository.saveSpcOutput(spcOutput, profileId, isActive, traderInput,
                updatedRecommendationId)).thenReturn(status);

        spcRecommendationOutputService.parseAndSaveSpcOutput(spcOutput, profileId, traderInput);
    }

    @Test
    public void gellAllSpcListTest() {
        int isActive = 1;
        List<SpcComputationOutputStore> spcList = new ArrayList<SpcComputationOutputStore>();
        SpcComputationOutputStore spcOutput = new SpcComputationOutputStore();

        spcList.add(spcOutput); 

        Mockito.when(spcComputationOutputRepository.getAllSpcOutputList(1)).thenReturn(spcList);

        List<SpcComputationOutputStore> spcListOutput = spcRecommendationOutputService.getAllSpcOutput(isActive);

        org.junit.Assert.assertNotNull(spcListOutput);
    }

}
