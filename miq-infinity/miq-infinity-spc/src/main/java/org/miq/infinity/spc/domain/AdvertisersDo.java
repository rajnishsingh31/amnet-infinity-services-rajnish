package org.miq.infinity.spc.domain;

import java.util.List;

public class AdvertisersDo implements Advertisers{
	List<Advertiser> advertisers;

	public List<Advertiser> getAdvertisers() {
		return advertisers;
	}

	public void setAdvertisers(List<Advertiser> advertisers) {
		this.advertisers = advertisers;
	}
	
	
}