package org.miq.infinity.spc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "commercial_input_store")

public class CommercialInputStore {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "profile_id")
    private int profileId;

    @Column(name = "media_owner")
    private String mediaOwner;

	@Column(name = "priority")
    private int priority;

    @Column(name = "create_date")
    // @Temporal(TemporalType.DATE)
    private Date createDate;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "isactive")
    private int isActive;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

  
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }
    
    
    public String getMediaOwner() {
		return mediaOwner;
	}

	public void setMediaOwner(String mediaOwner) {
		this.mediaOwner = mediaOwner;
	}

	@Override
	public String toString() {
		return "CommercialInputStore [id=" + id + ", profileId=" + profileId + ", priority=" + priority
				+ ", createDate=" + createDate + ", updateDate=" + updateDate + ", isActive=" + isActive
				+ ", mediaOwner=" + mediaOwner + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + id;
		result = prime * result + isActive;
		result = prime * result + ((mediaOwner == null) ? 0 : mediaOwner.hashCode());
		result = prime * result + priority;
		result = prime * result + profileId;
		result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommercialInputStore other = (CommercialInputStore) obj;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (id != other.id)
			return false;
		if (isActive != other.isActive)
			return false;
		if (mediaOwner == null) {
			if (other.mediaOwner != null)
				return false;
		} else if (!mediaOwner.equals(other.mediaOwner))
			return false;
		if (priority != other.priority)
			return false;
		if (profileId != other.profileId)
			return false;
		if (updateDate == null) {
			if (other.updateDate != null)
				return false;
		} else if (!updateDate.equals(other.updateDate))
			return false;
		return true;
	}
  
	
	

}
