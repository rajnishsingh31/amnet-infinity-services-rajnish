package org.miq.infinity.spc.domain;

public interface Campaign {
	
	int getCampaignId();
	String getCampaignName();
}
