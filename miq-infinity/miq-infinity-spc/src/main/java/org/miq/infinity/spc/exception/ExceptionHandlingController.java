package org.miq.infinity.spc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice("basePackages[com.miq.infinity.spc.*")
@RestController
//@ControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<SpcExceptionResponse> resourceNotFound(ResourceNotFoundException ex) {
        SpcExceptionResponse response = new SpcExceptionResponse();
        response.setErrorCode("Not Found");
        response.setErrorMessage(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<SpcExceptionResponse> badRequest(BadRequestException ex) {
        SpcExceptionResponse response = new SpcExceptionResponse();
        response.setErrorCode("Bad Request");
        response.setErrorMessage(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataBaseAccessException.class)
    public ResponseEntity<SpcExceptionResponse> internalServerError(Exception ex) {
        SpcExceptionResponse response = new SpcExceptionResponse();
        response.setErrorCode("Internal server error");
        response.setErrorMessage(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<SpcExceptionResponse> dataNotFound(DataNotFoundException ex) {
        SpcExceptionResponse response = new SpcExceptionResponse();
        response.setErrorCode("Data Not Found");
        response.setErrorMessage(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
