package org.miq.infinity.spc.controller.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.miq.infinity.spc.controller.SpcController;
import org.miq.infinity.spc.domain.AdvertisersDo;
import org.miq.infinity.spc.domain.Campaign;
import org.miq.infinity.spc.domain.CampaignDo;
import org.miq.infinity.spc.domain.CampaignsDo;
import org.miq.infinity.spc.domain.CommInputCreteriaImpl;
import org.miq.infinity.spc.domain.DeleteMediaOwnerDo;
import org.miq.infinity.spc.domain.InsertionOrdersDo;
import org.miq.infinity.spc.domain.MediaOwner;
import org.miq.infinity.spc.domain.MediaOwnersDo;
import org.miq.infinity.spc.domain.SPCRecommendationsAndTradersInputDo;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.engine.CommercialMediaOwnersService;
import org.miq.infinity.spc.engine.DbmAdvertiserIoCampaignDataService;
import org.miq.infinity.spc.engine.SpcRecommendationOutputService;
import org.miq.infinity.spc.engine.SpcTraderService;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.miq.infinity.spc.repository.CommercialInputStoreDao;
import org.miq.infinity.spc.util.SpcUtil;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpcControllerTest {

	private static final String SUPPLY_PARTNER_CONTROLS = "/supply-partner-controls/";

	private static final Log LOGGER = LogFactory.getLog(SpcControllerTest.class);

	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	private MockMvc mockMvc;

	@Mock
	 private CommercialMediaOwnersService commercialMediaOwnersService;

	@Mock
    private SpcRecommendationOutputService spcRecommendationOutputService;

	@Mock
    private SpcTraderService spcTraderService;
    
	@Mock
    private DbmAdvertiserIoCampaignDataService dbmAdvertiserIoCampaignDataService;
    
	@Mock 
    SpcUtil spcUtil;

	@Mock
    CommercialInputStoreDao commercialInputStoreDao;
	
	@InjectMocks
	SpcController spcController;

	static final class Uri {
		private Uri() {
		}

		public static final String MEDIA_OWNER_PRIORITIES = "/media-owners/priorities";
		public static final String MEDIA_OWNER_PRIORITIES_FILE_UPLOAD = "/media-owners/upload";
		public static final String MEDIA_OWNER = "/media-owner";
		public static final String CAMPAIGN_RECOMMENDATION_PROVIDER = "/campaign-recommendation-provider";
		public static final String CAMPAIGN_RECOMMENDATIONS = "/campaign-recommendations";
		public static final String MEDIA_OWNERS_LOOKUP = "/media-owners-lookup";
		public static final String DBM_ADVERTISERS_LOOKUP = "/dbm-advertisers";
		public static final String DBM_IO_LOOKUP = "/dbm-insertion-orders";
		public static final String DBM_CAMPAIGNS_LOOKUP = "/dbm-campaigns";
	}

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(spcController).build();
	}

	@Test
	public void getMediaOwnersAndPrioritiesTest() throws Exception {
		List<CommercialInputStore> commList = new ArrayList<CommercialInputStore>();
		CommercialInputStore commInput = new CommercialInputStore();
		Date createDate = new Date();
		commInput.setMediaOwner("Test.com");
		commInput.setPriority(1);
		commInput.setProfileId(10);
		commInput.setCreateDate(createDate);
		commInput.setUpdateDate(createDate);
		commInput.setIsActive(1);
		commList.add(commInput);
		Mockito.when(commercialMediaOwnersService.getMediaOwnersAndAssociatedPriorities()).thenReturn(commList);
		Mockito.when(spcUtil.isCollectionEmpty(commList)).thenReturn(false);
		
		try{
			mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNER_PRIORITIES).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
		} catch (Exception e) {
			LOGGER.error(e);

		}
	}
	
	
	@Test
	public void getMediaOwnersAndPrioritiesFailureTest() throws Exception {
		List<CommercialInputStore> commList = new ArrayList<CommercialInputStore>();
		CommercialInputStore commInput = new CommercialInputStore();
		Date createDate = new Date();
		commInput.setMediaOwner("Test.com");
		commInput.setPriority(1);
		commInput.setProfileId(10);
		commInput.setCreateDate(createDate);
		commInput.setUpdateDate(createDate);
		commInput.setIsActive(1);
		commList.add(commInput);
		Mockito.when(spcUtil.isCollectionEmpty(commList)).thenReturn(true);
		Mockito.when(commercialMediaOwnersService.getMediaOwnersAndAssociatedPriorities()).thenReturn(commList);
		try{
			mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNER_PRIORITIES).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
		} catch (Exception e) {
			LOGGER.error(e);

		}
	}
	

	@Test
	public void updateMediaOwnerTest() throws Exception {
		CommInputCreteriaImpl commInputCreteriaImplInput = new CommInputCreteriaImpl();
		commInputCreteriaImplInput.setDomain("a.com");
		commInputCreteriaImplInput.setId(2);
		commInputCreteriaImplInput.setPriority(3);
		ObjectMapper mapper = new ObjectMapper();
		String input = mapper.writeValueAsString(commInputCreteriaImplInput);
		String commInputCreteriaImplOutput = "UPDATE_OUTPUT";
		Mockito.when(commercialMediaOwnersService.updateCommercialInput(commInputCreteriaImplInput))
				.thenReturn(commInputCreteriaImplOutput);
		mockMvc.perform(put(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNER).contentType(APPLICATION_JSON_UTF8).content(input))
				.andExpect(status().isOk());
	}

	@Test
	public void deleteMediaOwnerTest() throws Exception {
		int id = 1;
		DeleteMediaOwnerDo deletionMessage = new DeleteMediaOwnerDo();
		Mockito.when(commercialMediaOwnersService.deleteMediaOwner(id)).thenReturn(deletionMessage);
		mockMvc.perform(delete(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNER+"/1").contentType(APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
	}

	@Test
	public void processTraderInputsAndGenerateCampaignRecommendationTest() throws Exception {
		SPCRecommendationsAndTradersInputDo sPCRecommendationsAndTradersInputDo = new SPCRecommendationsAndTradersInputDo();
		Date startDate = new Date();
		List<String> creativeSize = new ArrayList<>();
		List<String> deviceType = new ArrayList<>();
		deviceType.add("Desktop");
		creativeSize.add("300*500");
		TraderInputCriteriaImpl traderInput = new TraderInputCriteriaImpl();
		traderInput.setAdvertiserId(1234);
		traderInput.setBudget(9000);
		traderInput.setCampaignId(4567);
		traderInput.setCampaignType("Retargetting");
		traderInput.setCtrGoal("0.02");
		traderInput.setCreativeSize(creativeSize);
		traderInput.setImpressions(900000);
		traderInput.setStartdate(startDate);
		traderInput.setEnddate(startDate);
		ObjectMapper mapper = new ObjectMapper();
		String input = mapper.writeValueAsString(traderInput);
		String status = "SUCCESS";
		Mockito.when(spcTraderService.processTraderInput(traderInput)).thenReturn(sPCRecommendationsAndTradersInputDo);
		mockMvc.perform(post(SUPPLY_PARTNER_CONTROLS+Uri.CAMPAIGN_RECOMMENDATION_PROVIDER).contentType(APPLICATION_JSON_UTF8).content(input))
				.andExpect(status().isOk());
	}

	@Test
	public void getAllSpcRecommendationsTest() throws Exception {
		List<SpcComputationOutputStore> spcList = new ArrayList<SpcComputationOutputStore>();
		SpcComputationOutputStore spcOutput = new SpcComputationOutputStore();
		Date createDate = new Date();
		int isActive = 1;
		spcOutput.setCampaignId(1234);
		spcOutput.setCampaignType("rtg");
		spcOutput.setCreationDate(createDate);
		spcOutput.setDomain("a.com");
		spcOutput.setExpectedCtr(0.03);
		spcOutput.setExpectedImpressions(90000);
		spcOutput.setExpectedViewability(">10%");
		spcOutput.setId(1);
		spcOutput.setIsActive(1);
		spcOutput.setProfileId(2);
		spcList.add(spcOutput);
		Mockito.when(spcRecommendationOutputService.getAllSpcOutput(isActive)).thenReturn(spcList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.CAMPAIGN_RECOMMENDATIONS).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}
	
	
	@Test
	public void getMediaOwnersFromLookupTest() throws Exception{
		List<MediaOwner> mediaOwnersList = new ArrayList<>();
		Mockito.when(commercialMediaOwnersService.getMediaOwnersLookupData()).thenReturn(mediaOwnersList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNERS_LOOKUP).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}
	
	@Test
	public void getMediaOwnersFromLookupFailureTest() throws Exception{
		List<MediaOwner> mediaOwnersList = new ArrayList<>();
		Mockito.when(spcUtil.isCollectionEmpty(mediaOwnersList)).thenReturn(true);
		Mockito.when(commercialMediaOwnersService.getMediaOwnersLookupData()).thenReturn(mediaOwnersList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNERS_LOOKUP).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isNotFound());
	}
	
	@Test
	public void getDBMAdvertisersTest() throws Exception{
		AdvertisersDo  advertisersList= new AdvertisersDo();
		Mockito.when(dbmAdvertiserIoCampaignDataService.getDbmAdvertisers()).thenReturn(advertisersList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.DBM_ADVERTISERS_LOOKUP).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}
	
	@Test
	public void getDBMAdvertisersFailureTest() throws Exception{
		AdvertisersDo  advertisersList= new AdvertisersDo();
		Mockito.when(spcUtil.isCollectionEmpty(advertisersList.getAdvertisers())).thenReturn(true);
		Mockito.when(dbmAdvertiserIoCampaignDataService.getDbmAdvertisers()).thenReturn(advertisersList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.DBM_ADVERTISERS_LOOKUP).contentType(APPLICATION_JSON_UTF8)).andExpect(status().isNotFound());
	}
	
	
	@Test
	public void getDBMiosTest() throws Exception{
		InsertionOrdersDo  iosList= new InsertionOrdersDo();
		Mockito.when(dbmAdvertiserIoCampaignDataService.getDbmInsertionOrdersByAdvertiserId(1)).thenReturn(iosList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.DBM_IO_LOOKUP).param("advertiserId", "1").contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}
	
	@Test
	public void getDBMiosFailureTest() throws Exception{
		InsertionOrdersDo  iosList= new InsertionOrdersDo();
		Mockito.when(spcUtil.isCollectionEmpty(iosList.getInsertionOrders())).thenReturn(true);
		Mockito.when(dbmAdvertiserIoCampaignDataService.getDbmInsertionOrdersByAdvertiserId(1)).thenReturn(iosList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.DBM_IO_LOOKUP).param("advertiserId", "1").contentType(APPLICATION_JSON_UTF8)).andExpect(status().isNotFound());
	}
	
	@Test
	public void getDBMCampaignsTest() throws Exception{
		CampaignsDo  campaignsList= new CampaignsDo();
		List<Campaign> list = new ArrayList<>();
		CampaignDo cam = new CampaignDo();
		cam.setCampaignId(2);
		cam.setCampaignName("TEST_CAMPAIGN");
		list.add(cam);
		campaignsList.setCampaigns(list);
		Mockito.when(spcUtil.isCollectionEmpty(campaignsList.getCampaigns())).thenReturn(false);
		Mockito.when(dbmAdvertiserIoCampaignDataService.getDbmCampaignByInsertionOrderId(2)).thenReturn(campaignsList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.DBM_CAMPAIGNS_LOOKUP).param("insertionOrderId","2").contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}
	
	@Test
	public void getDBMCampaignsFailureTest() throws Exception{
		CampaignsDo  campaignsList= new CampaignsDo();
		List<Campaign> list = new ArrayList<>();
		CampaignDo cam = new CampaignDo();
		cam.setCampaignId(2);
		cam.setCampaignName("TEST_CAMPAIGN");
		list.add(cam);
		campaignsList.setCampaigns(list);
		Mockito.when(spcUtil.isCollectionEmpty(campaignsList.getCampaigns())).thenReturn(true);
		Mockito.when(dbmAdvertiserIoCampaignDataService.getDbmCampaignByInsertionOrderId(2)).thenReturn(campaignsList);
		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.DBM_CAMPAIGNS_LOOKUP).param("insertionOrderId","2").contentType(APPLICATION_JSON_UTF8)).andExpect(status().isNotFound());
	}
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Test
	public void uploadMediaOwnersCommercialFileTest() throws Exception {
		//MultipartFile uploadFile = null;
		MockMultipartFile uploadFile = new MockMultipartFile("file", "orig", null, "bar".getBytes());		MediaOwnersDo mediaOwnersDo = new MediaOwnersDo();
		MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		
		Mockito.when(commercialMediaOwnersService.uploadMediaOwnersCommercialFileData(uploadFile, "test")).thenReturn(mediaOwnersDo);
//		mockMvc.perform(get(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNER_PRIORITIES_FILE_UPLOAD).param("file","uploadFile").param("profilename", "test").contentType(APPLICATION_JSON_UTF8)).andExpect(status().isOk());
		 mockMvc.perform(MockMvcRequestBuilders.fileUpload(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNER_PRIORITIES_FILE_UPLOAD)
                 .file(uploadFile)
                 .param("profilename", "test"))
             .andExpect(status().is(400));
		
//		mockMvc.perform(post(SUPPLY_PARTNER_CONTROLS+Uri.MEDIA_OWNER_PRIORITIES_FILE_UPLOAD).file(uploadFile))
//		.andExpect("file");
	}
	

}
