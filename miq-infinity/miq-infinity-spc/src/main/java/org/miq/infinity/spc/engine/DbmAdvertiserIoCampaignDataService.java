package org.miq.infinity.spc.engine;

import org.miq.infinity.spc.domain.Advertisers;
import org.miq.infinity.spc.domain.Campaigns;
import org.miq.infinity.spc.domain.InsertionOrders;
import org.miq.infinity.spc.repository.DbmAdvertiserIOCampaignRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DbmAdvertiserIoCampaignDataService {
	
    private static final Logger LOG = LoggerFactory.getLogger(DbmAdvertiserIoCampaignDataService.class);
    
	@Autowired
	DbmAdvertiserIOCampaignRepository dbmAdvertiserIOCampaignRepository;
	
	public Advertisers getDbmAdvertisers(){
		LOG.info("Inside Service : Fetching DBM Advertisers");
		return dbmAdvertiserIOCampaignRepository.getDbmAdvertisers();
	}
	
	
	public InsertionOrders getDbmInsertionOrdersByAdvertiserId(int advertiserId){
		LOG.info("Inside Service : Fetching DBM InsertionOrders for advertiser : {}",advertiserId);
		return dbmAdvertiserIOCampaignRepository.getDbmInsertionOrdersByAdvertiserId(advertiserId);
	}
	
	
	public Campaigns getDbmCampaignByInsertionOrderId(int insertionOrderId){
		LOG.info("Inside Service : Fetching DBM Campaigns for insertion order: {}",insertionOrderId);
		return dbmAdvertiserIOCampaignRepository.getDbmCampaignByInsertionOrderId(insertionOrderId);
	}
	
	

}
