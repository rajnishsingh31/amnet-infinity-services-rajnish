package org.miq.infinity.spc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = 4285102939497561697L;

    public BadRequestException(String message) {
        super(message);

    }
    public BadRequestException(String message, Throwable cause) {
        super(message);

    }

}
