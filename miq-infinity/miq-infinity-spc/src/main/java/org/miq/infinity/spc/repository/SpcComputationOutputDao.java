package org.miq.infinity.spc.repository;

import java.util.List;

import org.miq.infinity.spc.entity.SpcComputationOutputStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SpcComputationOutputDao extends JpaRepository<SpcComputationOutputStore, Long> {

    @Query(value = "SELECT max(recommendation_identifier) from spc_computation_output_store", nativeQuery = true)
    public String findMaxRecommendationIdentifier();

    public List<SpcComputationOutputStore> findByIsActive(int isActive);
}
