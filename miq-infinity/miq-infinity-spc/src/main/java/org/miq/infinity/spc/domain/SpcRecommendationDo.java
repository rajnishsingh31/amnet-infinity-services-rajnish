package org.miq.infinity.spc.domain;

public class SpcRecommendationDo implements SpcRecommendation {
    private String domain;
    private double recommendedCpm;
    private double expectedCtr;
    private String expectedViewability;
    private long expectedImpressions;
    private String deviceType;

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setRecommendedCpm(double recommendedCpm) {
        this.recommendedCpm = recommendedCpm;
    }

    public void setExpectedCtr(double expectedCtr) {
        this.expectedCtr = expectedCtr;
    }

    public void setExpectedViewability(String expectedViewability) {
        this.expectedViewability = expectedViewability;
    }

    public void setExpectedImpressions(long expectedImpressions) {
        this.expectedImpressions = expectedImpressions;
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public double getRecommendedCpm() {
        return recommendedCpm;
    }

    @Override
    public double getExpectedCtr() {
        return expectedCtr;
    }

    @Override
    public String getExpectedViewability() {
        return expectedViewability;
    }

    @Override
    public long getExpectedImpressions() {
        return expectedImpressions;
    }

    @Override
    public String getDeviceType() {
        return deviceType;
    }

	@Override
	public String toString() {
		return "SpcComputationOutputDo [domain=" + domain + ", recommendedCpm=" + recommendedCpm + ", expectedCtr="
				+ expectedCtr + ", expectedViewability=" + expectedViewability + ", expectedImpressions="
				+ expectedImpressions + ", deviceType=" + deviceType + "]";
	}
    
    
    

}
