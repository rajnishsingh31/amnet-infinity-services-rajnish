package org.miq.infinity.spc.util.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.miq.infinity.spc.entity.CommercialInputStore;
import org.miq.infinity.spc.util.SpcUtil;
import org.mockito.InjectMocks;
import org.springframework.mock.web.MockMultipartFile;

class SpcUtilTest {

    @InjectMocks
    private SpcUtil spcUtil;

    List<CommercialInputStore> commList = null;

    @Before
    public void init() {

        commList = new ArrayList<CommercialInputStore>();
 
        CommercialInputStore commInput = new CommercialInputStore();

        Date createDate = new Date();
        commInput.setMediaOwner("Test.com");
        commInput.setPriority(1);
        commInput.setProfileId(10);
        commInput.setCreateDate(createDate);
        commInput.setUpdateDate(createDate);
        commInput.setIsActive(1);

        commList.add(commInput);
    }

    @Test
    public void parseUploadedExcelFileTest() {

        MockMultipartFile uploadfile = new MockMultipartFile("Commfile", "testCommInput.xlsx", "text/plain",
                "Spring Framework".getBytes());

    }
}
