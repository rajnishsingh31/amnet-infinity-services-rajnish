package org.miq.infinity.spc.domain;

public class DbmAdvertiserLookupDo implements DbmAdvertiserLookup{
	
	private long id;
	private long ioId;
	private long advertiserId;
	private String name;
	private String ioName;
	private String advertiserName;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIoId() {
		return ioId;
	}
	public void setIoId(long ioId) {
		this.ioId = ioId;
	}
	public long getAdvertiserId() {
		return advertiserId;
	}
	public void setAdvertiserId(long advertiserId) {
		this.advertiserId = advertiserId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIoName() {
		return ioName;
	}
	public void setIoName(String ioName) {
		this.ioName = ioName;
	}
	public String getAdvertiserName() {
		return advertiserName;
	}
	public void setAdvertiserName(String advertiserName) {
		this.advertiserName = advertiserName;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (advertiserId ^ (advertiserId >>> 32));
		result = prime * result + ((advertiserName == null) ? 0 : advertiserName.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (ioId ^ (ioId >>> 32));
		result = prime * result + ((ioName == null) ? 0 : ioName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DbmAdvertiserLookupDo other = (DbmAdvertiserLookupDo) obj;
		if (advertiserId != other.advertiserId)
			return false;
		if (advertiserName == null) {
			if (other.advertiserName != null)
				return false;
		} else if (!advertiserName.equals(other.advertiserName))
			return false;
		if (id != other.id)
			return false;
		if (ioId != other.ioId)
			return false;
		if (ioName == null) {
			if (other.ioName != null)
				return false;
		} else if (!ioName.equals(other.ioName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "DbmAdvertiserLookupDo [id=" + id + ", ioId=" + ioId + ", advertiserId=" + advertiserId + ", name="
				+ name + ", ioName=" + ioName + ", advertiserName=" + advertiserName + "]";
	}
	
	
}
