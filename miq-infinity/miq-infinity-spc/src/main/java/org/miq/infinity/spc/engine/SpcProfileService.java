package org.miq.infinity.spc.engine;

import org.miq.infinity.spc.repository.ProfileInputRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SpcProfileService {

    private static final Logger LOG = LoggerFactory.getLogger(SpcProfileService.class);

    @Autowired
    ProfileInputRepository profileInputRepository;

    public int getProfileId(String profileName) {
    	LOG.info("Retrieving ProfileId by Profile Name :{}",profileName);
        return profileInputRepository.getAndSaveProfile(profileName);
    }

}
