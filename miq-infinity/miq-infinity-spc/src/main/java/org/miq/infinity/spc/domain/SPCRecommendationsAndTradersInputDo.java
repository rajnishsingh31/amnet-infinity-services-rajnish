package org.miq.infinity.spc.domain;

import java.util.List;

public class SPCRecommendationsAndTradersInputDo implements SPCRecommendationsAndTradersInput{
	
	TraderInputCriteria traderInput;
	List<SpcRecommendationDo> spcRecommendations;
	public TraderInputCriteria getTraderInput() {
		return traderInput;
	}
	public void setTraderInput(TraderInputCriteria traderInput) {
		this.traderInput = traderInput;
	}
	public List<SpcRecommendationDo> getSpcRecommendations() {
		return spcRecommendations;
	}
	public void setSpcRecommendations(List<SpcRecommendationDo> spcRecommendations) {
		this.spcRecommendations = spcRecommendations;
	}
	@Override
	public String toString() {
		return "SPCRecommendationsAndTradersInputDo [traderInput=" + traderInput + ", spcRecommendations="
				+ spcRecommendations + "]";
	}
	
	
}