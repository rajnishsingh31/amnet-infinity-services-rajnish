package org.miq.infinity.spc.engine;

import java.io.IOException;

import org.miq.infinity.spc.domain.SPCRecommendationsAndTradersInputDo;
import org.miq.infinity.spc.domain.TraderInputCriteriaImpl;
import org.miq.infinity.spc.repository.CommercialInputStoreDao;
import org.miq.infinity.spc.repository.TraderInputRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SpcTraderService {

    private static final String DATA_SUCCESS = "Data saved successfully";
	private static final Logger LOG = LoggerFactory.getLogger(SpcTraderService.class);

    @Autowired
    CommercialInputStoreDao commercialInputStoreDao;

    @Autowired
    SpcRecommendationOutputService spcRecommendationOutputService;

    @Autowired
    SpcProfileService spcProfileService;

    @Autowired
    TraderInputRepository traderInputRepository;

    public SPCRecommendationsAndTradersInputDo processTraderInput(TraderInputCriteriaImpl traderInputCriteria) {
    	LOG.debug("inside processTraderInput()");
        String uploadStatus="";
        String profileName = traderInputCriteria.getProfileName();
        LOG.info("Inventory advisor inputs given by user: {}",traderInputCriteria.getProfileName());
        int profileId = spcProfileService.getProfileId(profileName);
        String traderInput = traderInputCriteria.toString();
        LOG.info("Information Provide by Trader to get recommendation is : {}",traderInput);
        SPCRecommendationsAndTradersInputDo sPCRecommendationsAndTradersInputDo  = spcRecommendationOutputService.generateSPCRecommendations(traderInputCriteria, profileId);
        if (sPCRecommendationsAndTradersInputDo!=null && !sPCRecommendationsAndTradersInputDo.getSpcRecommendations().isEmpty()) {
        	LOG.info("Successfully Generated Recommendations for Input {}"+traderInput);
        	LOG.info("Saving traders input {}",traderInputCriteria);
            uploadStatus = traderInputRepository.saveTraderInput(traderInputCriteria, profileId);
            if(uploadStatus.equals(DATA_SUCCESS)) {
            	LOG.info("Inside: SpcTraderService :processTraderInput:Trader input saved successfully");
            }else {
            	LOG.error("SpcTraderService : SpcTraderService : processTraderInputTrader input could not be saved successfully");
            }
            return sPCRecommendationsAndTradersInputDo;
        }else {
        	LOG.error("Some issue while generating SPC Recomendations for trader input {}",traderInputCriteria.toString());
        }
        if(LOG.isDebugEnabled()) {
            LOG.debug("Exiting processTraderInput()");
        }
        return sPCRecommendationsAndTradersInputDo;
       }
}
