package org.miq.infinity.spc.exception;

public class SpcExceptionResponse {

    private static final long serialVersionUID = -2476914400673872934L;

    private String errorCode;

    public SpcExceptionResponse() {
        super();
    }

    public SpcExceptionResponse(String errorCode, String errorMessage) {
        super();
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    private String errorMessage;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
