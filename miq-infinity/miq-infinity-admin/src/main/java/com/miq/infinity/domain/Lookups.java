/*
 * Author : Subhan Ansari
 */
package com.miq.infinity.domain;

import java.io.Serializable;
import java.util.List;

public class Lookups implements Serializable {

	private static final long serialVersionUID = 8065977108012244852L;

	private List<Lookup> lookupsList;

	public void addLookup(Lookup lookup) {
		this.lookupsList.add(lookup);
	}

	public List<Lookup> getLookup() {
		return lookupsList;
	}

	public void setLookup(List<Lookup> lookups) {
		this.lookupsList = lookups;
	}

}
