package org.miq.infinity.spc.domain;

public interface AppnexusAdvertiserLookup {

   long getAdvertiserId();
   long getIoId();
   long getLineItemId();
   long getPlacementId();
   String getAdvertiserName();
   String getIoName();
   String getLineItemName();
   String getPlacementName();
}
